exports.config = {
	allScriptsTimeout : 11000,

	specs : [ 'e2e/*.js' ],

	capabilities : {
		'browserName' : 'chrome',
		'chromeOptions' : {
			binary : 'C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe',
			args : [],
			extensions : [],
		}

	},

	baseUrl : 'http://localhost:8000/app/',

	framework : 'jasmine',

	jasmineNodeOpts : {
		defaultTimeoutInterval : 30000
	}
};
