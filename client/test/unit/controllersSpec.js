'use strict';

describe('Application controllers', function() {
	beforeEach(module('PP.controllers'));

	it('should contain NewsCtrl controller', inject(function($controller) {
		var ctrl = $controller('NewsCtrl', {
			$scope : {}
		});
		expect(ctrl).toBeDefined();
	}));

	describe('"Site under build" page', function() {
		var $httpBackend = null;
		var scope = null;

		beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
			$httpBackend = _$httpBackend_;
			scope = $rootScope.$new();
			var ctrl = $controller('ManAtWorkCtrl', {
				$scope : scope
			});

			expect(ctrl).toBeDefined();
		}));

		it('should POST the email to the server REST mailinglist service', function() {
			$httpBackend.whenPOST('/services/mailinglist').respond(201, '');

			scope.sendEmail('test.email@example.com');

			expect(scope.emailSent).not.toBe(true);
			$httpBackend.flush();
			expect(scope.emailSent).toBe(true);
		});

		it('should tell if the email has been already submitted', function() {
			$httpBackend.whenPOST('/services/mailinglist', function() {
				return true;
			}).respond(409, '');

			scope.sendEmail('duplicate.email@example.com');

			expect(scope.duplicateEmail).not.toBe(true);
			$httpBackend.flush();
			expect(scope.duplicateEmail).toBe(true);
		});

	});

});
