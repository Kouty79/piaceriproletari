describe('Flickr API', function() {
	beforeEach(module('Flickr.services'));

	var httpMock = null;
	var flickrApi = null;
	beforeEach(inject(function($httpBackend, _flickrApi_) {
		httpMock = $httpBackend;
		flickrApi = _flickrApi_;
	}));

	afterEach(function() {
		httpMock.verifyNoOutstandingExpectation();
		httpMock.verifyNoOutstandingRequest();
	});

	it('should should exist', function() {
		expect(flickrApi).toBeDefined();
	});

	it('should be able to get the photo sets list', function() {
		httpMock.expectGET(getFlickrUrlMatcher({
			method : 'flickr.photosets.getList'
		})).respond({
			"photosets" : {
				"page" : 1,
				"pages" : 1,
				"perpage" : 1,
				"total" : 1,
				"photoset" : [{
					"id" : "72157642392648593",
					"primary" : "13171280463",
					"secret" : "ef37e7ca96",
					"server" : "3666",
					"farm" : 4,
					"photos" : 1,
					"videos" : 0,
					"title" : {
						"_content" : "Prova"
					},
					"description" : {
						"_content" : "Set di prova"
					},
					"needs_interstitial" : 0,
					"visibility_can_see_set" : 1,
					"count_views" : 0,
					"count_comments" : 0,
					"can_comment" : 0,
					"date_create" : "1394904181",
					"date_update" : "1394904181"
				}]
			},
			"stat" : "ok"
		});

		var response = flickrApi.photosets.getList();
		httpMock.flush();

		expect(response.photosets.photoset[0].title._content).toBe('Prova');
	});

	it('should be able to get the photos in a photoset', function() {
		httpMock.expectGET(getFlickrUrlMatcher({
			method : 'flickr.photosets.getPhotos',
			photoset_id : '72157642392648593'
		})).respond({
			"photoset" : {
				"id" : "72157642392648593",
				"primary" : "13171280463",
				"owner" : "120525038@N08",
				"ownername" : "piaceriproletari",
				"photo" : [{
					"id" : "13171280463",
					"secret" : "ef37e7ca96",
					"server" : "3666",
					"farm" : 4,
					"title" : "Koala",
					"isprimary" : 0
				}],
				"page" : 1,
				"per_page" : "500",
				"perpage" : "500",
				"pages" : 1,
				"total" : 1,
				"title" : "Prova"
			},
			"stat" : "ok"
		});

		var response = flickrApi.photosets.getPhotos({
			photoset_id : '72157642392648593'
		});

		httpMock.flush();

		expect(response.photoset.photo[0].title).toBe('Koala');
	});

	it('should be able to return the photo url', function() {
		// https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
		var photo = {
			"id" : "13171280463",
			"secret" : "ef37e7ca96",
			"server" : "3666",
			"farm" : 4,
			"title" : "Koala",
			"isprimary" : 0
		};

		expect(flickrApi.url(photo, 'b')).toBe('https://farm4.staticflickr.com/3666/13171280463_ef37e7ca96_b.jpg');
	});

});