describe('FotoCtrl', function() {
	beforeEach(module('PP.foto'));

	var fotoCtrl = null;
	var scope = null;
	var httpMock = null;
	beforeEach(inject(function($rootScope, $controller, $httpBackend) {
		scope = $rootScope.$new();
		fotoCtrl = $controller('FotoCtrl', {
			$scope : scope
		});
		httpMock = $httpBackend;
	}));

	it('should load all the photos form flickr Foto set', function() {
		httpMock.expectGET(getFlickrUrlMatcher({
			method : 'flickr.photosets.getPhotos',
			photoset_id : fotoCtrl.FOTO_SET_ID
		})).respond({
			"photoset" : {
				"id" : "72157644770225449",
				"primary" : "14426009125",
				"owner" : "120525038@N08",
				"ownername" : "piaceriproletari",
				"photo" : [{
					"id" : "14426009125",
					"secret" : "e53ebd974f",
					"server" : "3924",
					"farm" : 4,
					"title" : "Tulips",
					"isprimary" : 1
				}, {
					"id" : "14424705042",
					"secret" : "58b7fa65f4",
					"server" : "3852",
					"farm" : 4,
					"title" : "Penguins",
					"isprimary" : 0
				}],
				"page" : 1,
				"per_page" : "500",
				"perpage" : "500",
				"pages" : 1,
				"total" : 2,
				"title" : "Foto"
			},
			"stat" : "ok"
		});

		httpMock.flush();

		expect(scope.photos.length).toBe(2);
	});

});