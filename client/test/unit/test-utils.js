function getFlickrUrlMatcher(params) {
	var SERVICES_URL = 'https://api.flickr.com/services/rest/';

	return function(url) {
		if (url.indexOf(SERVICES_URL) > 0) {
			return false;
		}

		for ( var param in params) {
			var urlParam = param + "=" + params[param];
			if (url.indexOf(urlParam) <= -1) {
				return false;
			}
		}

		return true;
	};
}