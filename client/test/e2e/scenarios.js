'use strict';

describe('PiaceriProletari App', function() {

	// describe('Rooting', function() {
	// /**
	// * <ul>
	// * <li>In order to see the Piaceri Proletari web site</li>
	// * <li>As a enthusiastic fan</li>
	// * <li>I want the web site to be easily navigable</li>
	// * </ul>
	// */
	// browser.get('index.html');
	//
	// it('should automatically redirect to /news when location hash/fragment is empty', function() {
	// /**
	// * <ul>
	// * <li>Given I googled 'piaceriproletari'</li>
	// * <li>When I click on the piaceri proletari web site</li>
	// * <li>Then I expect to enter the site home page</li>
	// * </ul>
	// */
	// expect(browser.getLocationAbsUrl()).toMatch("/news");
	// });
	// });
	//
	// describe('News page', function() {
	//
	// it('show the latest news.', function() {
	// /**
	// * <ul>
	// * <li>Given I am in the news page.</li>
	// * <li>When I look at the content</li>
	// * <li>Then I expect to see the latest news</li>
	// * </ul>
	// */
	//
	// var news = element.all(by.repeater('new in news'));
	// expect(news.count()).toBeGreaterThan(0);
	// });
	// });

	describe('"Sito in costruzione" page', function() {
		/**
		 * <ul>
		 * <li>In order to know that the site is under build</li>
		 * <li>As a enthusiastic fan</li>
		 * <li>I want to be told</li>
		 * <li>AND I want to be able to leave my email to know latest news</li>
		 * </ul>
		 */
		browser.get('index.html');

		it('must tell me that the site is under building', function() {
			/**
			 * <ul>
			 * <li>Given I loaded the Piaceri Proletari web site</li>
			 * <li>When I look at the content</li>
			 * <li>Then I expect to see 'Sito in costruzione' page</li>
			 * </ul>
			 */
			expect(element(by.id('man-at-work')).isPresent()).toBe(true);
		});

		// it('must let me submit my email', function() {
		// /**
		// * <ul>
		// * <li>Given I loaded the Piaceri Proletari web site</li>
		// * <li>When I submit my email</li>
		// * <li>Then I expect to see a confirmation that email has been sent</li>
		// * </ul>
		// */
		// });

	});

});
