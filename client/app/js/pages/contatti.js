'use strict';
angular.module('PP.pages.contatti', ['Flickr.services']);

angular.module('PP.pages.contatti').controller('ContattiCtrl', ['$scope', 'flickrApi', '$sce', function ($scope, flickrApi, $sce) {
    $scope.header.contattiPage.select();

    var FOTO_SET_ID = '72157646923591029';
    flickrApi.photosets.getPhotos({
        photoset_id: FOTO_SET_ID
    }, function (data) {
        $scope.photos = [];
        var photos = data.photoset.photo;

        for (var i = 0; i < photos.length; i++) {
            var photo = photos[i];
            $scope.photos.push({
                src: flickrApi.url(photo, 'b'),
                url: flickrApi.url(photo, 'b'),
                title: photo.title
            });
        }
    });

    flickrApi.photosets.getInfo({
        photoset_id: FOTO_SET_ID
    }, function (data) {
        $scope.info = {};
        $scope.info.title = data.photoset.title._content;
        $scope.info.description = $sce.trustAsHtml(data.photoset.description._content.replace(/\n/g, '<br>'));
    });
}]);
