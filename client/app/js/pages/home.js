'use strict';
angular.module('PP.pages.sitoInCostruzione', ['Flickr.services']);

(function() {
	angular.module('PP.pages.sitoInCostruzione').controller('SitoInCostruzioneCtrl',
			['$scope', '$http', 'flickrApi', SitoInCostruzioneCtrl]);

	function SitoInCostruzioneCtrl($scope, $http, flickrApi) {
		$scope.header.homePage.select();
		$scope.emailSent = false;
		$scope.duplicateEmail = false;

		var FOTO_SET_ID = '72157646924735107';
		flickrApi.photosets.getPhotos({
			photoset_id : FOTO_SET_ID
		}, function(data) {
			var photo = data.photoset.photo[0];
			$scope.logoUrl = flickrApi.url(photo, 'b');
		});

		var DUPLICATE_STATUS = 409;

		$scope.sendEmail = function(email) {
			if (!email || email == '') {
				console.log('email non valida');
				alert('Non è una email valida!');
				return;
			}

			$http.post('/services/mailinglist', {
				email : email
			}).success(function() {
				$scope.emailSent = true;
			}).error(function(data, status, headers, config) {
				$scope.emailSent = false;

				if (status == DUPLICATE_STATUS) {
					$scope.duplicateEmail = true;
				}
			});
			;

		};

	}
})();