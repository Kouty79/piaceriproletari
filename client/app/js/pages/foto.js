'use strict';

angular.module('PP.pages.foto', ['Flickr.services']);

angular.module('PP.pages.foto').controller('FotoCtrl', ['$scope', 'flickrApi', '$sce', function ($scope, flickrApi, $sce) {
    $scope.header.fotoPage.select();

    var FOTO_SET_ID = '72157644770225449';
    flickrApi.photosets.getPhotos({
        photoset_id: FOTO_SET_ID
    }, function (data) {
        $scope.photos = [];
        var photos = data.photoset.photo;

        for (var i = 0; i < photos.length; i++) {
            var photo = photos[i];
            $scope.photos.push({
                src: flickrApi.url(photo, 'n'),
                url: flickrApi.url(photo, 'b'),
                title: photo.title
            });
        }
    });

    flickrApi.photosets.getInfo({
        photoset_id: FOTO_SET_ID
    }, function (data) {
        $scope.info = {};
        $scope.info.title = data.photoset.title._content;
        $scope.info.description = $sce.trustAsHtml(data.photoset.description._content.replace(/\n/g, '<br>'));
    });

}]);