/**
 * Created by Kouty on 08/03/2015.
 */

'use strict';
angular.module('PP.pages.biografia', ['Flickr.services']);

angular.module('PP.pages.biografia').controller('BiografiaCtrl', ['$scope', 'flickrApi', '$sce', function ($scope, flickrApi, $sce) {
    $scope.header.biografiaPage.select();

    var FOTO_SET_ID = '72157642392648593';
    flickrApi.photosets.getPhotos({
        photoset_id: FOTO_SET_ID
    }, function (data) {
        $scope.photos = [];
        var photos = data.photoset.photo;

        for (var i = 0; i < photos.length; i++) {
            var photo = photos[i];
            $scope.photos.push({
                src: flickrApi.url(photo, 'b'),
                url: flickrApi.url(photo, 'b'),
                title: photo.title
            });
        }
    });

    flickrApi.photosets.getInfo({
        photoset_id: FOTO_SET_ID
    }, function (data) {
        $scope.info = {};
        $scope.info.title = data.photoset.title._content;
        $scope.info.description = $sce.trustAsHtml(data.photoset.description._content.replace(/\n/g, '<br>'));
    });
}]);
