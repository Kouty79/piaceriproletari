'use strict';
angular.module('PP', ['ngRoute', 'PP.filters', 'PP.services', 'PP.directives', 'PP.controllers', 'PP.pages.foto',
    'PP.pages.sitoInCostruzione', 'PP.pages.concerti', 'PP.pages.nonluoghi', 'PP.pages.biografia', 'PP.pages.contatti',
    'PP.pages.media']);

(function () {
    angular.module('PP').config(['$routeProvider', config]);

    function config($routeProvider) {
        $routeProvider.when('/news', {
            templateUrl: 'partials/news.html',
            controller: 'NewsCtrl'
        });
        $routeProvider.when('/foto', {
            templateUrl: 'partials/foto-list.html',
            controller: 'FotoCtrl'
        });
        $routeProvider.when('/concerti', {
            templateUrl: 'partials/foto-list.html',
            controller: 'ConcertiCtrl'
        });
        $routeProvider.when('/contatti', {
            templateUrl: 'partials/contatti.html',
            controller: 'ContattiCtrl'
        });
        $routeProvider.when('/media', {
            templateUrl: 'partials/media.html',
            controller: 'MediaCtrl'
        });
        $routeProvider.when('/nonluoghi', {
            templateUrl: 'partials/non-luoghi.html',
            controller: 'NonluoghiCtrl'
        });
        $routeProvider.when('/biografia', {
            templateUrl: 'partials/biografia.html',
            controller: 'BiografiaCtrl'
        });
        $routeProvider.when('/home', {
            templateUrl: 'partials/home.html',
            controller: 'SitoInCostruzioneCtrl'
        });
        $routeProvider.otherwise({
            redirectTo: '/home'
        });
    }
})();

(function () {
    angular.module('PP').controller('AppController', ['$scope', AppController]);

    function AppController($scope) {
        $scope.header = {};

        $scope.header.homePage = {
            select: function () {
                $scope.header._page = 'homePage';
            },
            isSelected: function () {
                return $scope.header._page === 'homePage';
            }
        };

        $scope.header.fotoPage = {
            select: function () {
                $scope.header._page = 'fotoPage';
            },
            isSelected: function () {
                return $scope.header._page === 'fotoPage';
            }
        };

        $scope.header.nonluoghiPage = {
            select: function () {
                $scope.header._page = 'nonluoghiPage';
            },
            isSelected: function () {
                return $scope.header._page === 'nonluoghiPage';
            }
        };

        $scope.header.biografiaPage = {
            select: function () {
                $scope.header._page = 'biografiaPage';
            },
            isSelected: function () {
                return $scope.header._page === 'biografiaPage';
            }
        };

        $scope.header.concertiPage = {
            select: function () {
                $scope.header._page = 'concertiPage';
            },
            isSelected: function () {
                return $scope.header._page === 'concertiPage';
            }
        };

        $scope.header.contattiPage = {
            select: function () {
                $scope.header._page = 'contattiPage';
            },
            isSelected: function () {
                return $scope.header._page === 'contattiPage';
            }
        };

        $scope.header.mediaPage = {
            select: function () {
                $scope.header._page = 'mediaPage';
            },
            isSelected: function () {
                return $scope.header._page === 'mediaPage';
            }
        };
    }
})();