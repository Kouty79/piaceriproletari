/**
 * Flickr angular REST API
 */
'use strict';

angular.module('Flickr.services', ['ngResource']);

(function () {
    function flickr($resource) {
        // https://api.flickr.com/services/rest/?method=...&api_key=...&user_id=...&format=json&nojsoncallback=1
        var flickr = $resource('https://api.flickr.com/services/rest/?method=:method', {
            method: '@method',
            api_key: '9ba4495e6ce616318854be0f88cf91d1',
            user_id: '120525038@N08',
            format: 'json',
            nojsoncallback: 1
        });

        flickr.photosets = {};

        flickr.photosets.getInfo = function (params, success, error) {
            var requestParams = angular.extend({
                method: 'flickr.photosets.getInfo'
            }, params);

            return flickr.get(requestParams, success, error);
        };

        flickr.photosets.getList = function () {
            return flickr.get({
                method: 'flickr.photosets.getList'
            });
        };

        flickr.photosets.getPhotos = function (params, success, error) {
            var requestParams = angular.extend({
                method: 'flickr.photosets.getPhotos'
            }, params);

            return flickr.get(requestParams, success, error);
        };

        flickr.url = function (photo, photoSize) {
            var size = photoSize;
            var url = 'https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg';

            if (size == null) {
                size = '';
            } else {
                size = '_' + size;
            }

            return url.replace('{farm-id}', photo.farm).replace('{server-id}', photo.server).replace('{id}', photo.id)
                .replace('{secret}', photo.secret).replace('_[mstzb]', size);

        };

        return flickr;
    }

    angular.module('Flickr.services').service('flickrApi', ['$resource', flickr]);
}());